# OScH_for_disaster_monitoring_and_decision_support

Repository for issue #92 of the GOSH roadmap.

This repository will be used to track the development of the project related to
the use and development of Open Source Hardware tools for disaster monitoring and 
decision support.

This project started as suggested by @saibhaskarnakka during the 2018 edition of
[GOSH](http://openhardware.science/).
